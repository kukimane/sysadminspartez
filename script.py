#!/usr/bin/python3.5
import subprocess
import json
import sys
import collections
import operator
import copy

repoName = sys.argv[1]
TOP = sys.argv[2]
LIMIT = sys.argv[3]

userNumber = 0
commitNumber = 0
userSlugNumber = 0
sumTop = 0
slugs = []
users = {}
usersTmp = {}
userCommitSumMax = []
userCommitSumDict = {}


getSlugsCurl="curl https://api.bitbucket.org/2.0/repositories/"+repoName+"/"
process = subprocess.Popen(getSlugsCurl.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
output = output.decode("utf-8") 
getSlugsJson = json.loads(output)

while True:
    try:
        getSlugsJson['values'][userSlugNumber]
        slugs.append(getSlugsJson['values'][userSlugNumber]['slug'])
        userSlugNumber += 1
    except:
        break

for s in slugs:
    getUsersCurl="curl https://api.bitbucket.org/2.0/repositories/"+repoName+"/"+s+"/commits"
    process = subprocess.Popen(getUsersCurl.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output = output.decode("utf-8") 
    getUsersJson = json.loads(output)
    commitNumber = 0
    while True:
        try:
            getUsersJson['values'][commitNumber]
            if getUsersJson['values'][commitNumber]['author']['user']['display_name'] not in users:
                users[getUsersJson['values'][commitNumber]['author']['user']['display_name']] = {"git_repos" : {s:1}}
            else:
                users[getUsersJson['values'][commitNumber]['author']['user']['display_name']]["git_repos"][s] += 1
            commitNumber += 1
            print (commitNumber)
            if commitNumber >= LIMIT:
                break
        except:
            break

for x in users:
    userCommitSum = 0
    users[x]['total_commits_share_percentage'] = 0
    for y in users[x]['git_repos']:
        userCommitSum += users[x]['git_repos'][y]
        users[x]['total_commits_share_percentage'] += userCommitSum
        
users={repoName:users}

for repo in users:
    for user, userSub in users[repo].items():
        userCommitSumDict[user] = userSub['total_commits_share_percentage']

od = collections.OrderedDict(sorted(userCommitSumDict.items()))
for i in range(0, int(TOP)):
    userCommitSumMax.append(max(od.items(), key=operator.itemgetter(1))[0])
    del od[max(od.items(), key=operator.itemgetter(1))[0]]

usersTmp = copy.deepcopy(users)
for x in usersTmp[repoName]:
    flag = 0
    for y in userCommitSumMax:
        if y == x:
            flag = 1
    if flag == 0:
        del users[repoName][x]

for x in users[repoName]:
    sumTop += users[repoName][x]['total_commits_share_percentage']

for x in users[repoName]:
    users[repoName][x]['total_commits_share_percentage'] = "{0:.2f}".format(100*(users[repoName][x]['total_commits_share_percentage']/sumTop))
    
print(users)
